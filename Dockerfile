# Dockerfile for simple-express-server
FROM node:14

WORKDIR /app

# Copy package.json and lock file
COPY packages/simple-express-server/package.json ./
COPY packages/simple-express-server/package-lock.json ./

# Install dependencies
RUN npm install

# Copy the rest of the application code
COPY packages/simple-express-server/ .

# Expose the necessary port
EXPOSE 3001

# Start the application
CMD ["node", "dist/server.js"]

